import dataHygrometrieJson from "../datas/hygrometrie.json";

const dataHygrometrie: Hygrometrie = dataHygrometrieJson.hygrometry;

export interface hygrometrieValue {
    "type": number;
    "value": number;
}

export interface Hygrometrie {
    "surface": hygrometrieValue[]
}

export default dataHygrometrie;

