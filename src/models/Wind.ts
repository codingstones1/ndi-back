import dataWindJson from "../datas/windforce.json";

const dataWind: Wind = dataWindJson.wind;

export interface windValue {
    "date": number;
    "value": any;
    "direction":String;
}

export interface Wind {
    "surface": windValue[]
}

export default dataWind;


