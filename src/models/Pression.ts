import dataPressionJson from "../datas/pression.json";

const dataPression: Pression = dataPressionJson.pression;

export interface pressionValue {
    "date": number;
    "value": number;
}

export interface Pression {
    "type": String;
    "values": pressionValue[];
}



export default dataPression;
