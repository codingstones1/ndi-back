import dataTemperatureJson from "../datas/temperature.json";

const dataTemperature: Temperatures = dataTemperatureJson.temperatures;

export interface temperatureValue {
    "date": number;
    "value": number;
}

export interface Temperatures {
    "surface": temperatureValue[];
    "altitude": temperatureValue[];
}

// export interface Temperature {
//     "temperatures" : Temperatures;
// }


export default dataTemperature;



