import * as mongoose from 'mongoose';
import { Request, Response } from 'express';
import dataTemperature from '../models/Temperature';
import dataPression from '../models/Pression';
import dataOxygen from '../models/Wind';
import dataHygrometrie from '../models/Hygrometrie';
import dataWind from '../models/Wind';



export class MeteoController{
    

    public getTemperatures (req: Request, res: Response) {           
            res.send(dataTemperature);
        
    }
    public getPressions (req: Request, res: Response) {   
        let pressions = dataPression.values;           
        res.send(pressions);
    }
    public getAltitudeTemperature (req: Request, res: Response) { 
        let tempA = dataTemperature.altitude[0];         
        res.send(tempA);
    }
    public getSurfaceTemperature (req: Request, res: Response) {    
        let tempS = dataTemperature.surface[0];
        res.send(tempS);
    }
    public getPression (req: Request, res: Response) {
        let pression = dataPression.values[0];
        res.send(pression);
    }
    public getWind (req: Request, res: Response) {    
        let wind = dataWind.surface[0];            
        res.send(wind);
    }
    public getHygrometrie (req: Request, res: Response) {
        let hydrometrie = dataHygrometrie.surface[0];
        res.send(hydrometrie);
    }

    public getWinds (req: Request, res: Response) {    
        let winds = dataWind.surface;            
        res.send(winds);
    }
    public getHygrometries (req: Request, res: Response) {
        let hydrometries = dataHygrometrie.surface;
        res.send(hydrometries);
    }

}
