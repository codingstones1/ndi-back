import { Request, Response } from "express";
import dataTemperature from "../models/Temperature";

/**
 * GET /
 * Home page.
 */
export let index = (req: Request, res: Response) => {
  res.json(dataTemperature.surface[0]);
};
