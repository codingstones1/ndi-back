import express from "express";
import cors from "cors";
import {MeteoController} from './controllers/meteoController'

// Controllers (route handlers)
import * as homeController from "./controllers/home";

// Create Express server
const app = express();
app.options('*', cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    next();
});



app.set("port", process.env.PORT || 8080);

app.get('/', homeController.index);

app.get('/products/test', cors(), function (req, res, next) {
    res.json({data: 'This is CORS-enabled for all origins!'})
});


/**
 * Primary app routes.
 */
// app.get("/", homeController.index);
// app.get("/login", userController.getLogin);
// app.post("/login", userController.postLogin);
// app.get("/logout", userController.logout);
// app.get("/forgot", userController.getForgot);
// app.post("/forgot", userController.postForgot);
// app.get("/reset/:token", userController.getReset);
// app.post("/reset/:token", userController.postReset);
// app.get("/signup", userController.getSignup);
// app.post("/signup", userController.postSignup);
// app.get("/contact", contactController.getContact);
// app.post("/contact", contactController.postContact);
// app.get("/account", passportConfig.isAuthenticated, userController.getAccount);
// app.post("/account/profile", passportConfig.isAuthenticated, userController.postUpdateProfile);
// app.post("/account/password", passportConfig.isAuthenticated, userController.postUpdatePassword);
// app.post("/account/delete", passportConfig.isAuthenticated, userController.postDeleteAccount);
// app.get("/account/unlink/:provider", passportConfig.isAuthenticated, userController.getOauthUnlink);

const meteoController = new MeteoController();
app.route("/meteo/temperatures" ).get(meteoController.getTemperatures);
app.route("/meteo/pressions" ).get(meteoController.getPressions);
app.route("/meteo/temperature/surface/last" ).get(meteoController.getSurfaceTemperature);
app.route("/meteo/temperature/altitude/last" ).get(meteoController.getAltitudeTemperature);
app.route("/meteo/pression/last" ).get(meteoController.getPression);
app.route("/meteo/wind/last" ).get(meteoController.getWind);
app.route("/meteo/hygrometrie/last" ).get(meteoController.getHygrometrie);
app.route("/meteo/winds" ).get(meteoController.getWinds);
app.route("/meteo/hygrometries" ).get(meteoController.getHygrometries);


export default app;